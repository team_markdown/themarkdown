mkdir -p build
rm -rf build/*
for i in `ls *.md`
do
    f="$(basename $i .md)"
    touch build/$f.html
    cat _header.html > build/$f.html
    pandoc $f.md -o $f.html
    cat $f.html >> build/$f.html
    rm $f.html
    cat _footer.html >> build/$f.html
done
