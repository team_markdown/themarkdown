## Welcome to The Markdown!

> by Matt Lee, May 1st 2019

A warm welcome to The Markdown, a new publication that will teach you
Markdown, while also telling you about the exciting world of secure
messaging, encrypted email, password managers, Yubikeys and associated
devices, VPNs, setting up servers, scraping websites, browser
developer tools and basic SQL. We also hope to have some journalism.

Submissions are very welcome -- in Markdown -- to
<markdown@well.com>. Our website is built entirely using a shell
script, and frankly it shows. We should probably use Hugo or something
modern, but for now it's a shell script.

So what is The Markdown about? Well, we're going to have a section on
tacos, I'll be reviewing every episode of Law and Order SVU starting
next season, and whatever anyone else sends in I suppose.

Once we have some regular content, you'll be able to download issues
of The Markdown (converted from Markdown to PDF, thanks to pandoc!)
and many of our regular features will be updated here on the website.

Journalism is important, of course, and so I would like to have a lot
of that on the site. Oh, and everything published here is published
under Creative Commons Attribution-ShareAlike, so please reuse and
remix The Markdown articles into your own publications, and give us
credit in the process.

---

Matt Lee is an artist, writer and director living in Boston, MA. He
hails from the UK, and previously worked for Creative Commons and the
Free Software Foundation. Follow him on Twitter @mattl.
